const http = require('http')
const express = require('express')
const socketIO = require('socket.io')
const BodyParser = require('body-parser')

let app = express()
let server = http.createServer(app)
let io = socketIO(server)

app.use(express.json())

app.use(BodyParser.urlencoded({ extended: true }))

let Twit = require('twit')

let TwiteerClint = new Twit({
  consumer_key: 'HlEmMglQgaUlXdsi0f3n7Bmsy',
  consumer_secret: 'QfNbOMIkraaS700ogUbjqrPYbLQ2PcHntAB4KwlhD4p4JXkUXN',
  access_token: '1239493661286531072-Ya7jfJMeRqsQwzbNeg3AXt9FGoUuV6',
  access_token_secret: '1aFNEnSba7sdxrT5b6D3uQ6K7R6IsCNu4MQC9F9c5SdU8'
})

let twitterStream
let searchTerm = ''

const startTwitterStream = () => {

  if (twitterStream == null && searchTerm && searchTerm.length) {
    twitterStream = TwiteerClint.stream('statuses/filter', { track: searchTerm ,language: 'en'})
    twitterStream.on('tweet', function (tweet) {
      io.emit('newTweet', tweet)
    })

  }
}

const stopTwitterStream = () => {
  twitterStream && twitterStream.stop()
  twitterStream = null
}

app.post('/updateSearchTerm', (req, res) => {

  searchTerm = req.body.searchTerm
  // res.status(200).send({ searchTerm: searchTerm })
  stopTwitterStream()
  startTwitterStream()

})

app.get('/search-tweets', (req, res) => {

  let { q = '', limit = 25} = req.query
  q = decodeURIComponent(q)
  TwiteerClint.get('search/tweets', { q, count: limit }, function(error, tweets, response) {
    if (error) return res.status(400).send({status: 400, message: error.message});
    res.status(200).json({ tweets })
  })
})

io.on('connection', (socket) => {
  startTwitterStream()
  socket.on('disconnect', () => {
    if (Object.keys(io.sockets.sockets).length === 0) {
      stopTwitterStream()
    }
  })
})

module.exports.server = server.listen(3001, () => {
  console.log('Server is up on port 3001')
})
module.exports.app = app
