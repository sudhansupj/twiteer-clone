import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ShareIcon from '@material-ui/icons/ShareOutlined';
import RepeatIcon from '@material-ui/icons/Repeat';
import FavoriteIcon from '@material-ui/icons/FavoriteBorderOutlined';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import ModeCommentOutlinedIcon from '@material-ui/icons/ModeCommentOutlined';
import {Box} from '@material-ui/core';
import Divider from "@material-ui/core/Divider";

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2),
    marginTop: theme.spacing(2),
    borderRadius: 15
  },
}));

const TwitListItem = ({eachTwit}) => {

  const classes = useStyles();

  const {user,text} = eachTwit;

  return (
      <Card className={classes.root}>
        <CardHeader
            avatar={
                <Avatar src={user.profile_image_url}/>
            }
            action={
              <IconButton aria-label="settings">
                <MoreVertIcon />
              </IconButton>
            }
            title={user && user.name ? user.name : 'Unknown User'}
            subheader={<a href={`https://twitter.com/${user.screen_name}`}  >{`@${user.screen_name}`}</a>}
        />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p">
            {text}
          </Typography>
        </CardContent>
          <Divider />
        <Box component={CardActions} justifyContent="space-around">
          <IconButton aria-label="Comment" size="small">
            <ModeCommentOutlinedIcon />
          </IconButton>
          <IconButton aria-label="Repeat" size="small">
            <RepeatIcon />
          </IconButton>
          <IconButton aria-label="Favorite" size="small">
            <FavoriteIcon />
          </IconButton>
          <IconButton aria-label="share" size="small">
            <ShareIcon />
          </IconButton>
        </Box>
      </Card>
  );
};

export default TwitListItem;