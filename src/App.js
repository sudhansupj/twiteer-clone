import React, { useEffect, useState} from 'react'
import { withAlert } from 'react-alert'
import socketIOClient from 'socket.io-client'
import axios from 'axios'
import Container from '@material-ui/core/Container';
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import TwitListItem from "./components/TwitListItem";
import AppBar from "./components/NavBar";
import {connect} from 'react-redux';
import * as tweetsAction from './redux/action/tweets.action';


function App ({ alert,tweet, tweetNotification,handleUpdateNotification,handleSetTweets}){

  // const [tweets,setTweets] = useState([]);
  const [term,setTerm] = useState([]);


  useEffect(() =>{
      const socket = socketIOClient('http://localhost:3000/')

      socket.on('connect', () => {
        socket.on('newTweet', (tweet) => {
          if(tweet)  handleUpdateNotification(tweetNotification + 1)
        })
      })
      socket.on('disconnect', () => {
        socket.off('newTweet')
        socket.removeAllListeners('newTweet')
      })
  },[])


  const updateSearchTerm = async () => {
      if(!term && !term.trim().length) return  alert.show('Please write something to search')
      handleSetTweets([]);
      handleUpdateNotification(0);
    axios.get(`/search-tweets?q=${term}`
    )
        .then(function ({data}) {
            handleSetTweets(data.tweets.statuses);
        })
        .catch(function (error) {
          console.log(error);
        });
   await axios.post('/updateSearchTerm', {searchTerm: term})
  }

    return (
        <>
          <AppBar tweetNotification={tweetNotification}/>
        <Container maxWidth="md">
          <Card  variant="outlined" style={{marginTop: '38px',padding: '9px',display: 'flex',alignItems: 'center'}}>
            <TextField
                id='searchInput'
                onChange={(e) => setTerm(e.target.value)} autoComplete='off' fullWidth id="outlined-basic" variant="outlined" />
            <Button
                onClick={updateSearchTerm}
                variant="contained"
                color="secondary"
                style={{marginLeft: '30px',height: '46px'}}>
              Search
            </Button>
          </Card>
          {
              tweet && tweet.map((each,i) =>
              <TwitListItem
                  eachTwit={each}
                  key={i}
              />
            )
          }
        </Container>
          </>
    )

}
const mapStateToProps = store => {
    return {
        tweet: store.tweets.tweet,
        tweetNotification: store.tweets.tweetNotification,
    };
};
const mapDispatchToProps = dispatch => ({
    handleUpdateNotification :(value)=>dispatch(tweetsAction.handleUpdateNotification(value)),
    handleSetTweets :(value)=>dispatch(tweetsAction.handleSetTweets(value)),
});

export default  connect(
    mapStateToProps,
    mapDispatchToProps
)(withAlert(App));


// export default withAlert(App)