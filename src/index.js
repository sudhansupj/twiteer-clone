import React, { Component } from 'react'
import { render } from 'react-dom'
import { Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'
import App from './App'
import { Provider } from "react-redux";
import reducer from './redux/reducers/Store';

// alert cofiguration
const options = {
  position: 'bottom center',
  timeout: 5000,
  offset: '30px',
  transition: 'scale'
}

class Root extends Component {
  render () {
    return (
      <AlertProvider template={AlertTemplate} {...options}>
        <Provider store={reducer}>
        <App />
        </Provider>
      </AlertProvider>
    )
  }
}

render(<Root />, document.getElementById('root'))
