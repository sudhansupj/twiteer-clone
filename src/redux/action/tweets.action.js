import {SET_NOTIFICATION, SET_TWEETS} from '../constants/reader.constants';

export const handleUpdateNotification = number => {
    return {
        type: SET_NOTIFICATION,
        number
    };
};
export const handleSetTweets = data => {
    return {
        type: SET_TWEETS,
        data
    };
};
