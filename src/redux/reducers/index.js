import { combineReducers } from 'redux';
import tweets from './tweets.reducers';

const reducer = combineReducers({
  tweets
});

export default reducer;
