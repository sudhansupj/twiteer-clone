import {SET_NOTIFICATION, SET_TWEETS} from '../constants/reader.constants';

const expense = (
    state = {
        tweet: [],
        tweetNotification: 0
    },
    action
) => {
    switch (action.type) {
        case SET_NOTIFICATION:
            return {
                ...state,
                tweetNotification: action.number
            };
        case SET_TWEETS:
            return {
                ...state,
                tweet: action.data
            };
        default:
            return state
    }
};

export default expense;
