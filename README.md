## Twitter Feed App

This repository hosts source code for simple application which allows you to see Twitter feed in real time.  
You can specify search term for your feed.  



The app was built using React and Node. For real time updates this app uses socket.io.

## Installation instructions

- Install [npm/node](https://nodejs.org/en/)
- Install the dependencies, run the following command from the root folder:
```sh
$ npm install
```
- Update server/server.js file  Twitter API key
- Run the server with the following command(browser will be opened automatically):
```sh
$ npm start
```

## Other usefull commands
  
- To start only back-end run the following command
```sh
$ npm run start-back-end
```
- To start only front-end run the following command
```sh
$ npm start-front-end
```


## Main tools and libraries used

- [React](https://reactjs.org/) - A JavaScript library for building user interfaces.
- [Material UI](https://material-ui.com/) - UI library for react.
- [Node.js](https://nodejs.org/en/) - A JavaScript runtime built on Chrome's V8 JavaScript engine.
- [Express](https://expressjs.com/) - Node.js web application framework.
- [socket.io](https://socket.io/) - The fastest and most reliable real-time engine.

